const path = require(`path`)

module.exports = {
  pathPrefix: `/dirpen/oraculus-portal-web/`,
  plugins: [

    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `assets`,
        path:  `${__dirname}/src/pages/views/assets`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `resources`,
        path:  `${__dirname}/src/components/tablePhase/resources`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `resources`,
        path:  `${__dirname}/src/components/layout/resources`,
      },
    },
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sass`,
  ] 
}