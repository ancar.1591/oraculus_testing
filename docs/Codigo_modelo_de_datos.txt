
Table Op_Estadistica {
  id_oe int 
  nombre_oe varchar
  id_periodo int
  fecha_publicacion date
}

Table periodo {
  id_periodo int
  nombre varchar
  codigo varchar
}

Table indicador_OE {
  id_oe int 
  id_indicador int
  order int
}

Table fase {
  id_fase int [primary key]
  nombre varchar
  id_oe varchar
  porcentaje int
  id_estado varchar
}

Table hito {
  id_hito int [primary key]
  nombre_hito varchar
  fecha_planeada date
  fecha_ejecucion  date
  id_responsable int
  nombre_fase varchar
  id_estado varchar
  diferencial int
  
  
}

Table indicador {
  id_indicador int [primary key]
  nombre_indicador varchar
  id_hito int
  valores valor_indicador
  sufijo varchar
  formula varchar
}

Table responsable {
  id_responsable int [primary key]
  nombres varchar
  apellidos varchar
  correo varchar
}

Table valor_indicador {
  id_indicador int
  id_periodo int
  valor int
}

Table estado {
  id_estado int
  nombre_estado int

}



Ref {
  Op_Estadistica.id_periodo > periodo.id_periodo
}

Ref {
  fase.id_oe > Op_Estadistica.id_oe
}

Ref {
  indicador_OE.id_oe > Op_Estadistica.id_oe
}

Ref {
  indicador_OE.id_indicador > indicador.id_indicador
}


Ref {
  hito.nombre_fase > fase.nombre
}

Ref {
  hito.id_responsable > responsable.id_responsable
}

Ref{
  indicador.id_indicador > valor_indicador.id_indicador
}

Ref {
  valor_indicador.id_periodo > periodo.id_periodo
}

Ref {
  indicador.id_hito > hito.id_hito
}

Ref {
  hito.id_estado > estado.id_estado
}

Ref {
  fase.id_estado > estado.id_estado
}


