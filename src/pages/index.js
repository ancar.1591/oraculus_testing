import React from 'react'
import PropTypes from 'prop-types'

// ----- load styles by material-ui ----------- //

import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';

// ----- layout components ----------- //

import HeaderComponent from "../components/layout/headerComponent"

// ----- load components ----------- //

import PlanningComponent from "../components/planning/planningComponent"
import CalendarComponent from "../components/calendar/calendarComponent"
import SelectOperationComponent from "../components/selectOperation/selectOperationComponent"
import SelectDependenceComponent  from "../components/selectDependence/selectDependenceComponent"
import TablePhaseComponent from "../components/tablePhase/tablePhaseComponent"


const drawerWidth = 340;
const styles = theme => ({

  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  root: {
    display: 'flex',
  },
  appBar: {
   
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  toolbar: theme.mixins.toolbar,
  content: {
    marginTop: 120,
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing(3),
  },
});


class IndexPage extends React.Component {


  constructor(props) {
    super(props);
    this.state = {
      idDonut: 'donutComponet1',
      idDonutA: 'donutComponent2',
      selectedDate: '12/08/2019',
      currentDay: new Date(),
      labelOperation: 'Seleccione la operación estadística',
      labelDependence: 'Seleccione una dependencia',
      selectOperation: null,
      selectDependence: null,
      currentOperation: null,
      currentDependence: null,
      clean: null,
      url: [
        { label: "Enero", count: 16 },
        { label: "Febrero", count: 12 },
        { label: "Marzo", count: 15 },
        { label: "Abril", count: 14 },
        { label: "Mayo", count: 11 },
        { label: "Junio", count: 13 }
      ],
      dataset: [
        { label: "Enero", count: 16 },
        { label: "Febrero", count: 12 }
      ],

    };

    this.handleChangeDate = this.handleChangeDate.bind(this);
    this.handleChangeOperation = this.handleChangeOperation.bind(this);
    this.handleChangeDependence = this.handleChangeDependence.bind(this);
    this.handleChangeClean = this.handleChangeClean.bind(this)
  }

  handleChangeDate(selectedDate) {
    console.log("emitiendo clicks", selectedDate)
    this.setState({ selectedDate: selectedDate })
  }

  handleChangeOperation(currentOperation) {
    console.log("index", currentOperation)
    this.setState({ currentOperation: currentOperation })

  }

  handleChangeDependence(currentDependence) {
    console.log("dependencia", currentDependence)
    this.setState({ currentDependence: currentDependence })
  }

  handleChangeClean(clean) {
    console.log("Borrar", clean)
    this.setState({ clean: clean })
  }


  render() {

    
    const { classes } = this.props;
    console.log("classses", classes)

    return (
      <>
        {/* 
          <PublicationComponent date={this.state.today}  />
          <DonutComponent id={this.state.idDonut} data={this.state.url} /> */}


        <div className={classes.root}>
          <CssBaseline />

          <AppBar className={classes.appBar} position="fixed" color="inherit" >
           <HeaderComponent /> 
          </AppBar>
          <Drawer
            className={classes.drawer}
            variant="permanent"
            classes={{
              paper: classes.drawerPaper,
            }}
            anchor="left">
            <Grid item
              container
              direction="row"
              justify="center"
              alignItems="center"
              id="container_logo">
              <img src="https://www.dane.gov.co/templates/t3_bs3_blank/images/logo-DANE.png" alt="Logo" className="logoDane"
              />
            </Grid>
            <Divider />
            <Grid id="titleCalendar"
              container
              direction="row"
              justify="center"
              alignItems="center">
              <h3>Hitos OOEE</h3>
            </Grid>
            <List>
              <CalendarComponent currentDate={this.state.selectedDate} onChangeDate={this.handleChangeDate} />
              <Divider />
              <Grid className="mt-2" ></Grid>
              <PlanningComponent currentDate={this.state.selectedDate} />
              <br />
            </List>
          </Drawer>
          <main className={classes.content}>
            {/*      <div className={classes.toolbar} /> */}
            <Paper >
              <Grid container
                direction="row"
                justify="center"
                alignItems="center"
                id="title_component">
                <span id="icoTitle"></span>
                <h2>Próximos hitos a cumplirse</h2>
              </Grid>
            </Paper>
            <Paper className="container_select">
              <Grid container spacing={3} direction="row" alignItems="flex-end">
                <Grid item xs={12} sm={6} container justify="center" className="select_position-1">
                  <SelectOperationComponent selectData={this.state.selectOperation} labelDesc={this.state.labelOperation} onChangeOperation={this.handleChangeOperation} />
                </Grid>
                <Grid item xs={12} sm={6} container justify="center" className="select_position-1" >
                  <SelectDependenceComponent selectData={this.state.selectDependence} labelDesc={this.state.labelDependence} onChangeDependence={this.handleChangeDependence} />

                  {/* <ButtonCleanComponent selectData={this.state.selectOperation}  onChangeOperation={this.handleChangeOperation}/> */}
                </Grid>
              </Grid>
            </Paper>
            <Paper className="mt-2">
              <TablePhaseComponent currentDate={this.state.selectedDate} operationValue={this.state.currentOperation} dependenceValue={this.state.currentDependence} />
            </Paper>

          </main>
        </div>

      </>
    )
  }
}

IndexPage.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};


export default withStyles(styles, {withTheme: true })(IndexPage);