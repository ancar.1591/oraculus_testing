import React  from 'react'
import * as d3 from 'd3'
import moment from 'moment'
import events from "../../models/events";

/* styles */
import '../planning/planningComponent.scss'


export default class ProccessComponent extends React.Component {
   
    componentDidUpdate(prevProps, prevState) {
       this.renderPlanning()
    }

   
    renderPlanning(){

        var that = this
        console.log("planning recibiendo", that.props.currentDate)

        var arraySelectedDate = [];
            arraySelectedDate = that.props.currentDate.split('/');
        var selectedMonth = arraySelectedDate[1]
        console.log("month", selectedMonth)

    
        var divEnter = d3.select("#planning").html("")
                        .selectAll("a")
                        .data(events.filter(function(d){ 
                                        var arrayDataDate = []
                                            arrayDataDate =  moment(new Date(d.end)).format("DD/MM/YYYY").split('/')
                                        var MonthDate = arrayDataDate[1]
                                            return MonthDate === selectedMonth;   
                                            }) 
                        )
                        .enter().append("a")
                        .on("click", function (d){ })
                     
                                        
        var label = divEnter.insert("span")
                        .attr('class', 'date')
                        .html(function(d) { 
                         return  moment(new Date(d.end)).format("DD/MM/YYYY")
                        })
       
       
                    label.insert("p")
                         .attr('class', "description")
                         .html(function(d) { 
                                if(d.fase === "Procesamiento"){
                                    d3.select(this).style('background-color', '#FFCB00').style('color', '#1A1A1A')
                                    return d.title         
                                } 
                                if(d.fase === "Recolección"){
                                    d3.select(this).style('background-color', '#456E7B').style('color', '#FFFFFF')
                                    return d.title
                                } 
                                if(d.fase === "Análisis"){
                                    d3.select(this).style('background-color', '#5B29AD').style('color', '#FFFFFF')
                                    return d.title
                                } 
                                if(d.fase === "Difusión"){
                                    d3.select(this).style('background-color', '#FF3E7D').style('color', '#FFFFFF')
                                    return d.title
                                }        
                            }).style("border-radius", '5px')    
            
                       /*  label.insert("span")
                             .data(events)
                             .attr('class', 'date')
                             .html(function(d) { return d.planningDate;}) 
        */
    }


    componentDidMount(nextProps) {
        this.renderPlanning();
    }


    render(){
        return (
            <div>
		            <div id="planning"></div>
            </div>
            
	    )
    }

}