import React  from 'react'
import * as d3 from 'd3'
import moment from 'moment'
import localization from 'moment/locale/es'
import {f} from 'd3-jetpack/essentials'

import events from "../../models/events"

import '../tablePhase/tablePhaseComponent.scss'



export default class TablePhaseComponent extends React.Component {
   
    componentDidUpdate(prevProps, prevState) {
        this.renderTablePhase()
    }

   
    renderTablePhase(){

     

      var columns = [
        { head: 'Fecha Planeada', cl: 'center', attribute: f('state'),  html:function(d){ return "&nbsp;&nbsp;" + moment(new Date(d.end)).locale("es", localization).format("LL")} },
        { head: 'Hito', cl: 'referentLink',  html:f('hito'), url: "hito/"}, 
        { head: 'Operación estadística', cl: 'referentLink', html:function(d){  return  d.abbreviation + " - " + d.reference}, url: "operation/"  },
        { head: 'Responsable', cl: 'center',  html:f('responsable')},
        { head: 'Fase', cl: f('fase'),  html: function (d) { return "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+d.fase } }

    ];

    var that = this
        console.log("tabla recibiendo de calendario", that.props.currentDate)
        console.log("tabla selector oper", that.props.operationValue)
        console.log("tabla selector dependence", that.props.dependenceValue)
    
    var arraySelectedDate = [];
        arraySelectedDate = that.props.currentDate.split('/');
    var selectedMonth = arraySelectedDate[1]
        console.log("array de fechas",arraySelectedDate)
        console.log("month", selectedMonth)
    // create table
    var table = d3.select('#tablePhase').html("")
        .append('table')
        .attr('class', 'width_table');
    // create table header
    table.append('thead').append('tr')
        .selectAll('th')
        .data(columns).enter()
        .append('th')
        .attr('class', f('cl'))
        .text(function(d){ return d.head })
       
    // create table body
    table.append('tbody')
        .selectAll('tr')
        .data(events.filter(function(d){ 
            var arrayDataDate = []
                arrayDataDate =  moment(new Date(d.end)).format("DD/MM/YYYY").split('/')
            var MonthDate = arrayDataDate[1] 
                    if(that.props.operationValue === null && that.props.dependenceValue === null){
                        return (MonthDate === selectedMonth && (d.state === "En Proceso" || d.state === "Atrasado"));
                    }
                    else if (that.props.operationValue === null){
                        return (MonthDate === selectedMonth && (d.dependence === that.props.dependenceValue) &&(d.state === "En Proceso" || d.state === "Atrasado"));
                    }
                    else if(that.props.dependenceValue === null){
                        return (MonthDate === selectedMonth && (d.abbreviation === that.props.operationValue) &&  (d.state === "En Proceso"  || d.state === "Atrasado"));
                    } 
                    else{ 
                        return (MonthDate === selectedMonth && (d.abbreviation === that.props.operationValue) && (d.dependence === that.props.dependenceValue) && (d.state === "En Proceso"  || d.state === "Atrasado") );  
                    }
                   
                }))
        .enter()
        .append('tr')
        .selectAll('td')
        .data(function(row, i) {
            return columns.map(function(c) {
                // compute cell values for this specific row
                var cell = {};
                d3.keys(c).forEach(function(k) {
                    cell[k] = typeof c[k] == 'function' ? c[k](row,i) : c[k];
                });
                return cell;
            });
        }).enter()
        .append('td')
        .attr('id', f('attribute'))
        .attr('class', f('cl'))
        .append('div')
        .attr('id',  f('cl'))
        .append('a')
        .html( f('html'))
        .attr("href", f('url'));
            
    }


    componentDidMount(nextProps) {
        this.renderTablePhase();
    }


    render(){
        return (
            <div>   
		        <div id="tablePhase"></div>  
            </div>
            
	    )
    }

}