import React from 'react';
import Select from 'react-select';

// styles
import '../selectDependence/selectDependence.scss'


import  dependence  from '../../models/dependence'


class SelectDependenceComponent extends React.Component {
  state = {
    selectedOption: null,
  };
  handleChange = selectedOption => {
    this.setState({ selectedOption });

    this.props.onChangeDependence(selectedOption.value)
  };
  render() {
    const { selectedOption } = this.state;
    
    return (
      <Select
        value={selectedOption}
        onChange={this.handleChange}
        options={dependence}
      />
    );
  }
}


export default SelectDependenceComponent