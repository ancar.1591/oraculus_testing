import React  from 'react'
import * as d3 from 'd3'

//import '../components/donut/donut.css'
//import * as utils from '../../helpers/utils'


export default class DonutComponent extends React.Component {

    
    componentDidUpdate(prevProps, prevState) {
        //this.renderDonutChart();
    }

    getDimensions(comp){
       
     
        d3.select('#'+this.props.id).selectAll("svg").remove(); 
        
        comp.height = 400;
       comp.width = 400;
       // comp.height = utils.widthCalc(this.props.id) - 50;
        //comp.width = utils.widthCalc(this.props.id);
       
       
         // a circle chart needs a radius
         comp.radius = Math.min(comp.width, comp.height) / 2;
         comp.donutWidth = 100; 

         // legend dimensions
         comp.legendRectSize = 15;
         comp.legendSpacing = 5; 

         // define color scale

         comp.color = d3.scaleOrdinal().range(["#E75B7F", "#2BC0B7", "#D8C35B", "#8F2ED7", "#1AAB3D", "#B7E6EA"])
      
          


         comp.svg = d3.select("#"+this.props.id).html("")
                        .append('svg')
                        .attr('width', comp.width)
                        .attr('height', comp.height) 
                        .append('g') 
                        .attr('transform', 'translate(' + (comp.width / 2) + ',' + (comp.height / 2) + ')'); 

         comp.arc = d3.arc()
                      .innerRadius(comp.radius - comp.donutWidth) 
                      .outerRadius(comp.radius); 

         comp.pie = d3.pie() 
                      .value(function(d) { return d.count; }) 
                      .sort(null);


    }   


    renderDonutChart(){

        
        var that = this;
       
        this.getDimensions(that);

     
        var dataset = that.props.data

         // calculate new total
         console.log("datos de donut ",dataset)
         var total = d3.sum(dataset, d => d.count);

         // define new total section
         var newTotal = d3.select('.new-total-holder')
                           .append('span')
                           .attr('class', 'newTotal')
                           .text(total);
               
        var tooltip = d3.select("#"+that.props.id)  
                        .append('div')                          
                        .attr('class', 'tooltip'); 

            tooltip.append('div') 
                    .attr('class', 'label'); 
            tooltip.append('div') 
                    .attr('class', 'count');                 
            tooltip.append('div') 
                    .attr('class', 'percent'); 

               

            dataset.forEach(function(d) {
                d.count = +d.count; 
                d.enabled = true;
            });

                // creating the chart
            var path = that.svg.selectAll('path') 
                          .data(that.pie(dataset)) 
                          .enter() 
                          .append('path') 
                          .attr('d', that.arc) 
                          .attr('fill', function(d) { return that.color(d.data.label); }) 
                        //.each(function(d) { that._current - d; }); 

               
                path.on('mouseover', function(d) {    
                that.total = d3.sum(dataset.map(function(d) {        
                return (d.enabled) ? d.count : 0;                                       
                }));                                                      
              
                tooltip.select('.label').html(d.data.label);                   
                tooltip.select('.percent').html(d.data.count + '%');          
                tooltip.style('display', 'block');                     
                });                                                           

                path.on('mouseout', function() {                      
                    tooltip.style('display', 'none'); 
                });

                path.on('mousemove', function(d) {                   
                    tooltip.style('top', (d3.event.layerY + 10) + 'px') 
                           .style('left', (d3.event.layerX + 10) + 'px'); 
                });

                // define legend
                var legend = that.svg.selectAll('.legend') 
                                    .data(that.color.domain())
                                    .enter()
                                    .append('g') 
                                    .attr('class', 'legend') 
                                    .attr('transform', function(d, i) {                   
                                        var height = that.legendRectSize + that.legendSpacing; 
                                        var offset =  height * that.color.domain().length / 2; 
                                        var horz = -2 * that.legendRectSize; 
                                        var vert = i * height - offset; 
                                        return 'translate(' + horz + ',' + vert + ')';       
                                    });

                // adding colored squares to legend
                legend.append('rect')                               
                      .attr('width', that.legendRectSize)                        
                      .attr('height', that.legendRectSize)                     
                      .style('fill', that.color) 
                      .style('stroke', that.color)
                      .on('click', function(label) {
                        var rect = d3.select(this); 
                        var enabled = true;
                        var totalEnabled = d3.sum(dataset.map(function(d) { 
                            return (d.enabled) ? 1 : 0; 
                        }));
                            if (rect.attr('class') === 'disabled') { 
                                rect.attr('class', ''); 
                            } else { 
                                if (totalEnabled < 2) return; 
                                    rect.attr('class', 'disabled'); 
                                    enabled = false; 
                            }

                    that.pie.value(function(d) { 
                    if (d.label === label) d.enabled = enabled; 
                        return (d.enabled) ? d.count : 0;
                    });

                    path = path.data(that.pie(dataset));

                    path.transition()
                    .duration(750) 
                    .attrTween('d', function(d) { 
                        var interpolate = d3.interpolate(this._current, d); 
                        this._current = interpolate(0); 
                        return function(t) {
                        return that.arc(interpolate(t));
                        };
                    });
                    
                    // calculate new total
                    var newTotalCalc = d3.sum(dataset.filter(function(d) { return d.enabled;}), d => d.count)
            
                
                    // append newTotalCalc to newTotal which is defined above
                    newTotal.text(newTotalCalc);
                });

                // adding text to legend
                legend.append('text')                                    
                .attr('x', that.legendRectSize + that.legendSpacing)
                .attr('y', that.legendRectSize - that.legendSpacing)
                .text(function(d) { return d; }); 

           // })      
            
    }


    componentDidMount(nextProps) {
        this.renderDonutChart();
        
    }

    componentWillUnmount(){
        this.renderDonutChart();
       
    }


    render(){
    
        return (
            <div>
		            <div  id={this.props.id}  ></div>
                   
            </div>
            
	    )
    }

}










