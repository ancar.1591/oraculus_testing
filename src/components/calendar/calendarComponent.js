import React from 'react';
import { Calendar, momentLocalizer } from 'react-big-calendar';
import moment from 'moment';

// load styles 
import '../calendar/calendar.scss'
//data calendar 
import events from "../../models/events";


moment.locale('es');
const localizer = momentLocalizer(moment);



export default class CalendarComponet extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      events,
      now: new Date(),
    };
  };

  eventStyleGetter(event, start, end, isSelected) {

    var backgroundColor;
    if (event.fase === "Difusión") {
      backgroundColor = '#FF3E7D';
    }
    if (event.fase === "Análisis") {
      backgroundColor = '#5B29AD';
    }
    if (event.fase === "Procesamiento") {
      backgroundColor = '#FFCB00';
    }
    if (event.fase === "Recolección") {
      backgroundColor = '#456E7B';
    }


    var style = {
      backgroundColor: backgroundColor,
      borderRadius: '0px',
      opacity: 0.8,
      color: 'black',
      border: '0px',
      display: 'block'
    };
    return {
      style: style
    };
  }

  render() {
    return (
      <div style={{ height: '45vh', color: "black", marginLeft: "20px" }}>
        <br></br>
        <Calendar
          style={{ height: 310, width: 270 }}
          step={60}
          events={this.state.events}
          startAccessor="start"
          endAccessor="end"
          defaultDate={this.state.now}
          localizer={localizer}
          views={['month']}
          onSelectEvent={event => alert("event")}
          onNavigate={(end) => { this.props.onChangeDate(moment(new Date(end)).format("DD/MM/YYYY")) }}
          eventPropGetter={(this.eventStyleGetter)}

          messages={{
            next: ">",
            previous: "<",
            month: "Mes",
            week: "Semana",
            day: "Día",
            today: ""     
          }}
        />
      </div>
    );
  }
}

