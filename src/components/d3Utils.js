export function translation(x, y) {return 'translate(' + x + ',' + y + ')';};
export function parentWidth(elem) {return elem.parentElement.clientWidth;};
export function widthCalc(id) {return this.parentWidth(document.getElementById(id));};
export function parentHeight(elem) {return elem.parentElement.clientHeight;};
export function heightCalc(id) {return this.parentHeight(document.getElementById(id));}; 
export function getCenter(elem) {
    var bbox = elem.node().getBBox();
    var center = [bbox.x + bbox.width / 2, bbox.y + bbox.height / 2];
    return { 'width': bbox.width, 'height': bbox.height, 'center': center };
};
export function dynamicSort(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
};
