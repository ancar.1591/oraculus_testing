export default [
    { label: "Ver todas", value: null },
    { label: "Gran Encuesta Integrada de Hogares (GEIH) ", value: "GEIH" },
    { label: "Encuesta Mensual Manufacturera Con Enfoque Territorial (EMMET)", value: "EMMET" },
    { label: "Encuesta Anual Manufacturera (EAM)", value: "EAM" }
]
